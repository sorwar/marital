

<!DOCTYPE HTML>
<html>
  <?php require_once('common/sessionStart.php') ?>
   <?php require_once('common/head.php') ?>
   <body>
      <!-- ============================  Navigation Start =========================== -->
      <?php require_once('common/header.php') ?>
      <!-- ============================  Navigation End ============================ -->
      <?php 
         require_once('mysql_connect.php');
         
         $q              = mysql_query("select * from contact where menuStatus = '1'");
         while ($r       = mysql_fetch_array($q)):
         $menuId          = $r['menuId'];
         $menuName        = $r['menuName'];
         $menuLink        = $r['menuLink'];
         $menuTitle       = $r['menuTitle'];
         $menuDis         = $r['menuDis'];
         $status          = $r['menuStatus'];
         
         
                            ?>
      <div class="grid_3">
         <div class="container">
            <div class="breadcrumb1">
               <ul>
                  <a href="index.html"><i class="fa fa-home home_1"></i></a>
                  <span class="divider">&nbsp;|&nbsp;</span>
                  <li class="current-page">Contact</li>
               </ul>
            </div>
            <div class="grid_5">
               <p><?php echo $menuDis?></p>
               <address class="addr row">
                  <dl class="grid_4">
                     <dt>Address :</dt>
                     <dd>
                        <?php echo $menuName ?>
                     </dd>
                  </dl>
                  <dl class="grid_4">
                     <dt>Telephones :</dt>
                     <dd style="width: 120px;">
                        <?php echo $menuLink ?>
                     </dd>
                  </dl>
                  <dl class="grid_4">
                     <dt>E-mail :</dt>
                     <dd><a href="#"><?php echo $menuTitle ?></a></dd>
                  </dl>
               </address>
            </div>
         </div>
      </div>
      <?php endwhile;?>
      <div class="about_middle">
         <div class="container">
            <h2>Contact Form</h2>
            <form id="contact-form" class="contact-form">
               <fieldset>
                  <input type="text" class="text" placeholder="" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
                  <input type="text" class="text" placeholder="" value="Phone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Phone';}">
                  <input type="text" class="text" placeholder="" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
                  <textarea value="Message" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                  <input name="submit" type="submit" id="submit" value="Submit">
               </fieldset>
            </form>
         </div>
      </div>
      <?php require_once('common/footer.php') ?>
   </body>
</html>

