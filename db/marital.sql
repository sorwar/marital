-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2017 at 01:16 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marital`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adminId` int(11) NOT NULL,
  `adminUsername` varchar(155) NOT NULL,
  `adminEmail` varchar(155) NOT NULL,
  `adminPassword` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminId`, `adminUsername`, `adminEmail`, `adminPassword`) VALUES
(1, 'admin', 'admin@merital.com', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `basic`
--

CREATE TABLE `basic` (
  `id` int(11) NOT NULL,
  `logo` varchar(155) NOT NULL,
  `about` text NOT NULL,
  `background` varchar(155) NOT NULL,
  `map` text NOT NULL,
  `facebook` varchar(155) NOT NULL,
  `twitter` varchar(155) NOT NULL,
  `google` varchar(155) NOT NULL,
  `youtube` varchar(155) NOT NULL,
  `headTitle` text NOT NULL,
  `footerTitle` text NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic`
--

INSERT INTO `basic` (`id`, `logo`, `about`, `background`, `map`, `facebook`, `twitter`, `google`, `youtube`, `headTitle`, `footerTitle`, `status`) VALUES
(30, '15006270341.png', 'Marital is one of the pioneers of online matrimony service. It is regarded as the most trusted matrimony website by Brand Trust Report. We have also been featured in Limca Book of records for having Record number of documented marriages online. Our purpose is to build a better Bharat through happy marriages. ', '15006270342.png', 'https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1825.0811813775513!2d90.42455788077953!3d23.812824815085474!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sbn!2sbd!4v1500626057441', 'https://www.facebook.com/sorwar121', 'https://twitter.com/sorwar121', 'https://plus.google.com', 'https://www.youtube.com/channel/UCqowCcLIUHZH-4FbHgYRwvg', 'Marital  | Metromonial Site', 'Â© copyright 2017 Merital', '1');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `menuId` int(11) NOT NULL,
  `menuName` varchar(155) NOT NULL,
  `menuLink` varchar(155) NOT NULL,
  `menuTitle` varchar(155) NOT NULL,
  `menuDis` text NOT NULL,
  `menuStatus` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`menuId`, `menuName`, `menuLink`, `menuTitle`, `menuDis`, `menuStatus`) VALUES
(3, '8901 Nulla Pariatur, Ipsum, D05 87GR.  hahaa', '     +880 175 033 0032     +880 800 789 5478 ', '     shossain@outlook.com ', 'Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient. montes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci ac sem. Duis ultricies pharetra magna. Donec accumsan malesuada orci. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit.', '1');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `memId` int(255) NOT NULL,
  `memProId` varchar(100) NOT NULL,
  `memUname` varchar(100) NOT NULL,
  `memPass` varchar(255) NOT NULL,
  `memEmail` varchar(255) NOT NULL,
  `memAgeDate` varchar(100) NOT NULL,
  `memAgeMonth` varchar(100) NOT NULL,
  `memAgeYear` varchar(100) NOT NULL,
  `memSex` varchar(100) NOT NULL,
  `memComm` varchar(255) NOT NULL,
  `memName` varchar(155) NOT NULL,
  `memMarital` varchar(155) NOT NULL,
  `memBody` varchar(155) NOT NULL,
  `memHeight` varchar(155) NOT NULL,
  `memPhysical` varchar(155) NOT NULL,
  `memCreated` varchar(155) NOT NULL,
  `memDrink` varchar(155) NOT NULL,
  `memAge` varchar(155) NOT NULL,
  `memTongue` varchar(155) NOT NULL,
  `memComplexion` varchar(155) NOT NULL,
  `memWeight` varchar(155) NOT NULL,
  `memBlood` varchar(155) NOT NULL,
  `memDiet` varchar(155) NOT NULL,
  `memSmoke` varchar(155) NOT NULL,
  `memBirthTime` varchar(155) NOT NULL,
  `memCaste` varchar(155) NOT NULL,
  `memBirthDate` varchar(155) NOT NULL,
  `memBirthPlace` varchar(155) NOT NULL,
  `memReligion` varchar(155) NOT NULL,
  `memSubCaste` varchar(155) NOT NULL,
  `memRaasi` varchar(155) NOT NULL,
  `memEdu` varchar(155) NOT NULL,
  `memEduDetail` varchar(155) NOT NULL,
  `memOccDetail` varchar(155) NOT NULL,
  `memAnnualIncome` varchar(155) NOT NULL,
  `memFaOcc` varchar(155) NOT NULL,
  `memMaOcc` varchar(155) NOT NULL,
  `memBroNum` varchar(155) NOT NULL,
  `memSisNum` varchar(155) NOT NULL,
  `memPartAge` varchar(155) NOT NULL,
  `memPartMarital` varchar(155) NOT NULL,
  `memPartBody` varchar(155) NOT NULL,
  `memPartComplexion` varchar(155) NOT NULL,
  `memPartHeight` varchar(155) NOT NULL,
  `memPartManglik` varchar(155) NOT NULL,
  `memPartReligion` varchar(155) NOT NULL,
  `memPartCaste` varchar(155) NOT NULL,
  `memPartTongue` varchar(155) NOT NULL,
  `memPartEducation` varchar(155) NOT NULL,
  `memPartOccupation` varchar(155) NOT NULL,
  `memPartCountry` varchar(155) NOT NULL,
  `memPartState` varchar(155) NOT NULL,
  `memPartResidency` varchar(155) NOT NULL,
  `memImage1` varchar(155) NOT NULL,
  `memImage2` varchar(155) NOT NULL,
  `memImage3` varchar(1555) NOT NULL,
  `memImage4` varchar(155) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`memId`, `memProId`, `memUname`, `memPass`, `memEmail`, `memAgeDate`, `memAgeMonth`, `memAgeYear`, `memSex`, `memComm`, `memName`, `memMarital`, `memBody`, `memHeight`, `memPhysical`, `memCreated`, `memDrink`, `memAge`, `memTongue`, `memComplexion`, `memWeight`, `memBlood`, `memDiet`, `memSmoke`, `memBirthTime`, `memCaste`, `memBirthDate`, `memBirthPlace`, `memReligion`, `memSubCaste`, `memRaasi`, `memEdu`, `memEduDetail`, `memOccDetail`, `memAnnualIncome`, `memFaOcc`, `memMaOcc`, `memBroNum`, `memSisNum`, `memPartAge`, `memPartMarital`, `memPartBody`, `memPartComplexion`, `memPartHeight`, `memPartManglik`, `memPartReligion`, `memPartCaste`, `memPartTongue`, `memPartEducation`, `memPartOccupation`, `memPartCountry`, `memPartState`, `memPartResidency`, `memImage1`, `memImage2`, `memImage3`, `memImage4`, `status`) VALUES
(101, 'MRS-001', 'sorwar', '202cb962ac59075b964b07152d234b70', 'sorwar3226@gmail.com', '24', 'September', '1994', 'Male', '', 'Md. sorwar Hossain', 'Single', '', '', '', '', '', '22', '', '', '', 'A+', '', '', '', '', '', 'Dhaka', 'Muslim', 'sunni', '', 'Diploma', '', 'Engineer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '15006434601.jpg', '15006434602.jpg', '15006434603.jpg', '15006434851.jpg', '1'),
(102, 'MRS-002', 'othoi', '202cb962ac59075b964b07152d234b70', 'keya@gmail.com', '17', 'March', '1994', 'Female', '', '', 'Single', '', '', '', '', '', '22', '', '', '', 'A+', '', '', '', '', '', 'Dhaka', 'Muslim', '', '', 'Diploma', 'Diploma', 'Teacher', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '15006439961.jpg', '15006439962.jpg', '15006439963.jpg', '15006439964.jpg', '1'),
(103, 'MRS-003', 'masum', '202cb962ac59075b964b07152d234b70', 'masum@gmail.com', '1', 'January', '1996', 'Male', '', 'Piyal Ahmed Masum', 'Single', '', '5ft 2 in', '', '', '', '22', '', '', '', 'A+', '', '', '', '', '', 'Dhaka', 'Muslim', '', '', 'Diploma', 'Diploma engineering', 'Engineer', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '15006992231.jpg', '', '', '', '1'),
(104, 'MRS-004', 'pranto', '202cb962ac59075b964b07152d234b70', 'pranto@gmail.com', '1', 'January', '1995', 'Male', '', 'Pranto kumar Roy', 'Single', '', '', '', '', '', '21', '', '', '', 'A+', '', '', '', '', '', 'Dhaka', 'hindu', '', '', 'Diploma', 'Diploma engineering', 'Doctor', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '15006994461.jpg', '', '', '', '1'),
(105, 'MRS-005', 'mishu', '202cb962ac59075b964b07152d234b70', 'mishu@gmail.com', '1', 'January', '1997', 'Male', '', 'Abul hasan mishu', 'Divorced', '', '5tf 10in', '', '', '', '21', '', '', '', 'A+', '', '', '', '', '', 'Dhaka', 'Muslim', '', '', 'Mbbs', 'Dhaka medical college', 'Doctor', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '15007006791.jpg', '', '', '', '1'),
(106, 'MRS-006', 'ananna', '202cb962ac59075b964b07152d234b70', 'sh@gmail.com', '11', 'February', '1995', 'Female', '', 'Salma Umme Vikarunnesa', 'Single', '', '', '', '', '', '20', '', '', '', '', '', '', '', '', '', 'Dhaka', 'Muslim', '', '', 'Diploma', 'Diploma engineering', 'Engineer', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '15007021351.jpg', '', '', '', '1'),
(107, 'MRS-007', 'kaniz', '202cb962ac59075b964b07152d234b70', 'kaniz@gmail.com', '19', 'January', '1996', 'Female', '', 'kaniz fatema', 'Single', '', '', '', '', '', '22', '', '', '', 'A+', '', '', '', '', '', 'Rajshahi', 'Muslim', '', '', 'Diploma', 'Diploma engineering', 'Engineer', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '15007026211.jpg', '', '', '', '1'),
(108, 'MRS-008', 'sobuj', '202cb962ac59075b964b07152d234b70', 'sh@gmail.com', '15', 'March', '1996', 'Male', '', '', 'Single', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menuId` int(11) NOT NULL,
  `menuName` varchar(155) NOT NULL,
  `menuLink` varchar(155) NOT NULL,
  `menuTitle` varchar(155) NOT NULL,
  `menuDis` text NOT NULL,
  `menuStatus` varchar(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menuId`, `menuName`, `menuLink`, `menuTitle`, `menuDis`, `menuStatus`) VALUES
(1, 'menu1', 'www.facebook.com', 'this is a menu', ' busdasdfhjklasffjkl skfkddj skfdkfh sdk nfdskfk sdnd snfdskds dkdnfdk sadnaskskl dkjsfnk sNFADSKKSH S', '0');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `msgId` int(155) NOT NULL,
  `msgBox` longtext NOT NULL,
  `msgSender` int(10) NOT NULL,
  `msgReceiver` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`msgId`, `msgBox`, `msgSender`, `msgReceiver`) VALUES
(26, 'hi baby', 54, 56),
(27, 'hi.... how are youuu......', 64, 54),
(28, 'hiiiii', 64, 56),
(29, 'hiiii', 64, 56),
(30, 'hii hridyyy', 64, 56),
(31, 'hiiii hpe', 65, 56),
(32, 'hiiiiii', 68, 70),
(33, 'hlwww..', 70, 68),
(34, 'hi bro', 68, 74),
(35, 'hlw bro', 74, 68),
(36, 'whats your name??', 68, 72),
(37, 'hi tata', 68, 73),
(38, 'hi there....', 68, 70),
(39, 'Firefox canâ€™t find the server at www.google.com.\r\n\r\n    Check the address for typing errors such as ww.example.com instead of www.example.com\r\n    If you are unable to load any pages, check your computerâ€™s network connection.\r\n    If your computer or network is protected by a firewall or proxy, make sure that Firefox Developer Edition is permitted to access the Web.', 68, 70),
(40, 'k re', 68, 69),
(41, 'hii ..how are you ?', 102, 101),
(42, 'hi there.... ', 105, 102);

-- --------------------------------------------------------

--
-- Table structure for table `submenu`
--

CREATE TABLE `submenu` (
  `subId` int(11) NOT NULL,
  `subName` varchar(155) NOT NULL,
  `subLink` varchar(155) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminId`);

--
-- Indexes for table `basic`
--
ALTER TABLE `basic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`menuId`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`memId`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menuId`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`msgId`);

--
-- Indexes for table `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`subId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `basic`
--
ALTER TABLE `basic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `menuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `memId` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `msgId` int(155) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `submenu`
--
ALTER TABLE `submenu`
  MODIFY `subId` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
