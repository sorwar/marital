<!DOCTYPE html>
<html lang="en">
    <body>
        <div>
            <?php require_once('common/sessionStart.php') ?>

            <?php require_once('common/notAcess.php') ?>

            <?php require_once('common/header.php') ?>

			<?php require_once('common/navList.php') ?>

			<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
				<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
			</div>
		</div>
		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Home</a>
						</li>

						<li>
							<a href="#">Contact</a>
						</li>
						<!-- <li class="active">List</li> -->
					</ul><!-- /.breadcrumb -->

					<div class="nav-search" id="nav-search">
						<form class="form-search">
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="ace-icon fa fa-search nav-search-icon"></i>
							</span>
						</form>
					</div><!-- /.nav-search -->
				</div>

				<div class="page-content">
					<!-- setting box -->
					<?php require_once('common/settings.php') ?>
					<!-- setting box end -->


					<div class="page-header">
						<h1>
							Contact
							<small>
								<i class="ace-icon fa fa-angle-double-right"></i>
								<!-- List -->
							</small>
						</h1>
					</div>

					<div class="row">
						<!-- Small Modal -->
					   <div class="modal fade" id="small" tabindex="-1" role="dialog" aria-hidden="true">
					                    <div class="modal-dialog modal-dialog-fromright">
					                        <div class="modal-content">
					                        	<div class="widget-box">
								        <div class="widget-header">
											<h4 class="widget-title">Menu Form</h4>

											<span class="widget-toolbar">
												<button class="close" type="button" data-dismiss="modal" aria-hidden="true">
		                                        <i class="ace-icon fa fa-times"></i>
		                                        </button>
											</span>
										 </div>
		                                 <form action="contact/action.php" method="post">
											<div class="widget-body">
												<div class="widget-main">
													<div>
														<label for="form-field-mask-1">
															Address :
														</label>

														<div class="input-group">
															<input class="input-large" type="text" id="register3-email" name="menuName">
														</div>
													</div>

													<hr>
													<div>
														<label for="form-field-mask-1">
															Phone :
														</label>

														<div class="input-group">
															<input class="input-large" type="text" id="register3-email" name="menuLink">
														</div>
													</div>

													<hr>
													<div>
														<label for="form-field-mask-1">
															Email :
														</label>

														<div class="input-group">
															<input class="input-large" type="text" id="register3-email" name="menuTitle">
														</div>
													</div>

													<hr>
													<div>
														<label for="form-field-mask-1">
															Description :
														</label>
														<div class="widget-box widget-color-blue">
															 <div class="widget-header widget-header-small">  </div>
															 <div class="widget-body">
															   <div class="widget-main no-padding">
															      <div class="md-editor" id="1500175451069">
															         <div class="md-header btn-toolbar">
															            <div class="btn-group"><button class="btn-default btn-sm btn btn-white" type="button" title="Bold (Ctrl+B)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdBold" data-hotkey="Ctrl+B"><span class="fa fa-bold"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Italic (Ctrl+I)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdItalic" data-hotkey="Ctrl+I"><span class="fa fa-italic"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Heading (Ctrl+H)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdHeading" data-hotkey="Ctrl+H"><span class="fa fa-header"></span> </button></div>
															            <div class="btn-group"><button class="btn-default btn-sm btn btn-white" type="button" title="URL/Link (Ctrl+L)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdUrl" data-hotkey="Ctrl+L"><span class="fa fa-link"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Image (Ctrl+G)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdImage" data-hotkey="Ctrl+G"><span class="fa fa-picture-o"></span> </button></div>
															            <div class="btn-group"><button class="btn-default btn-sm btn btn-white" type="button" title="Unordered List (Ctrl+U)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdList" data-hotkey="Ctrl+U"><span class="fa fa-list"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Ordered List (Ctrl+O)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdListO" data-hotkey="Ctrl+O"><span class="fa fa-list-ol"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Code (Ctrl+K)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdCode" data-hotkey="Ctrl+K"><span class="fa fa-code"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Quote (Ctrl+Q)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdQuote" data-hotkey="Ctrl+Q"><span class="fa fa-quote-left"></span> </button></div>
															            <div class="btn-group"><button class="btn-sm btn btn-primary btn-white" type="button" title="Preview (Ctrl+P)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdPreview" data-hotkey="Ctrl+P" data-toggle="button"><span class="fa fa-search"></span> Preview</button></div>
															            <div class="md-controls"><a class="md-control md-control-fullscreen" href="#"><span class="fa fa-expand"></span></a></div>
															         </div>
															         <textarea name="menuDis" data-provide="markdown" data-iconlibrary="fa" rows="6" class="md-input" style="resize: none;"></textarea>
															         <div class="md-fullscreen-controls"><a href="#" class="exit-fullscreen" title="Exit fullscreen"><span class="fa fa-compress"></span></a></div>
															         <div class="md-fullscreen-controls"><a href="#" class="exit-fullscreen" title="Exit fullscreen"><span class="fa fa-compress"></span></a></div>
															      </div>
															   </div>
															 </div>
													    </div>
													</div>
													<br>
													<div class="widget-toolbox padding-4 clearfix">
														<div class="btn-group pull-right">
															<button class="btn btn-sm btn-purple">
															  <i class="ace-icon fa fa-floppy-o bigger-125"></i>
																Save
															</button>
														</div>
													</div>
												</div>
											</div>
										 </form>
									 </div>                           
		                    	</div>
			                </div>
		                </div>
	          			 <!-- END Small Modal -->
					</div>

					<!-- PAGE CONTENT BEGINS -->
					<div class="row">
	                     <!-- form content  -->
	                    
						<!-- form content end  -->
						<div class="col-xs-12">
							<table id="simple-table" class="table  table-bordered table-hover">
								<thead>
									<tr>
										<th class="center">
											<label class="pos-rel">
												<input type="checkbox" class="ace" />
												<span class="lbl"></span>
											</label>
											</th>
											<th class="detail-col">No.</th>
												<th>Address</th>
												<th>Phone</th>
												<th>Email</th>
												<th>Description</th>
										<th class="hidden-480 text-center">Status</th>

										<th style="width: 100px;" class="hidden-480 text-center">Action</th>
									</tr>
								</thead>

								<?php 

									$i = 1;
									$j = 1;
									$k = 1;
									$m = 1;
									$s = 1;


									require_once('mysql_connect.php');

									$q           = mysql_query("select * from contact");
                                    while ($r    = mysql_fetch_array($q)):

                                  	 $menuId          = $r['menuId'];
                                  	 $menuName        = $r['menuName'];
                                 	 $menuLink        = $r['menuLink'];
               						 $menuTitle       = $r['menuTitle'];
               						 $menuDis         = $r['menuDis'];
                                     $status		  = $r['menuStatus'];


                                     ?>

								<tbody>
									<tr>
										<td class="center">
											<label class="pos-rel">
												<input type="checkbox" class="ace" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<a href="#"><?php echo $i++ ?></a>
										</td>
										<td>
											<a><?php echo $menuName ?></a>
										</td>
										<td><?php echo $menuLink ?></td>
										<td class="hidden-480"><?php echo $menuTitle ?></td>
										<td><?php echo $menuDis ?></td>
                                       <?php if ($status==1):?>
										<td class="hidden-480">
											<span class="label label-success arrowed-in arrowed-in-right">Active</span>
											<i style="color: green" class=""></i>
										</td>
									<?php else:?>
									    <td class="hidden-480">
											<span class="label label-danger arrowed-in">Inactive</span>
										</td>
									<?php endif;?>


										<td class="text-center">
											<div class="hidden-sm hidden-xs btn-group">
												

												<button class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal-fadein<?php echo $j++ ?>" >
													<i class="ace-icon fa fa-pencil bigger-120"></i>
												</button>
												&nbsp;

												<!-- <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-small<?php echo $s++ ?>" type="button" title="Remove Client" data-toggle="tooltip" href="#"><i class="fa fa-times"></i></a> -->

												
											</div>

											<div class="hidden-md hidden-lg">
												<div class="inline pos-rel">
													<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
														<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
													</button>

													<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
														<li>
															<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																<span class="blue">
																	<i class="ace-icon fa fa-search-plus bigger-120"></i>
																</span>
															</a>
														</li>

														<li>
															<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
																<span class="green">
																	<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																</span>
															</a>
														</li>

														<li>
															<a href="#modal-table" class="tooltip-error" data-rel="tooltip" title="Delete">
																<span class="red">
																	<i class="ace-icon fa fa-trash-o bigger-120"></i>
																</span>
															</a>
														</li>
													</ul>
												</div>
											</div>
										</td>
									</tr>
									<!-- Fade In Modal -->
                                    <div class="modal fade"  id="modal-fadein<?php echo $k++ ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="block block-themed block-transparent remove-margin-b">

		                                                <div class="modal-header no-padding">
															<div class="table-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																	<span class="white">&times;</span>
																</button>
																Update Form
															</div>
														</div>

												    
	                                                    <div class="block-content">
	                                                        <form class="form-horizontal push-10-t push-10" action="contact/update_action.php" method="post">
                                                                <div class="modal-body" style="height: 500px;">	
		                                                            <input type="hidden" value="<?php echo $menuId ?>"   name="menuId">
		                                                            <div class="form-group col-xs-12 ">
		                                                                <div class="col-xs-6">
		                                                                    <div class="form-material floating">
		                                                                        <label for="register3-username">Address :</label>
		                                                                        <input class="form-control" value="<?php echo $menuName ?>" type="text" id="register3-username" name="menuName">    
		                                                                    </div>
		                                                                </div>
		                                                                <div class="col-xs-6">
		                                                                    <div class="form-material floating">
		                                                                        <label for="register3-email"> Phone :</label>
		                                                                        <input class="form-control" value="<?php echo $menuLink ?>" type="text" id="register3-email" name="menuLink">
		                                                                    </div>
		                                                                </div>
		                                                            </div>
		                                                            <br>

		                                                            <div class="form-group col-xs-12">
		                                                                <div class="col-xs-6">
		                                                                    <div class="form-material floating">
		                                                                        <label for="register3-password">Email :</label>
		                                                                        <input class="form-control" value="<?php echo $menuTitle ?>" type="text" id="register3-password" name="menuTitle"> 
		                                                                    </div>
		                                                                </div>

		                                                                <br>
		                                                            </div>
									                                <div class="form-group col-xs-12">
									                                    <label for="register3-email">&nbsp; Description :</label>
									                                       <div class="col-xs-12">
									                                            <div class="widget-box widget-color-blue">
																				 <div class="widget-header widget-header-small">  </div>
																				 <div class="widget-body">
																				   <div class="widget-main no-padding">
																				      <div class="md-editor" id="1500175451069">
																				         <div class="md-header btn-toolbar">
																				            <div class="btn-group"><button class="btn-default btn-sm btn btn-white" type="button" title="Bold (Ctrl+B)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdBold" data-hotkey="Ctrl+B"><span class="fa fa-bold"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Italic (Ctrl+I)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdItalic" data-hotkey="Ctrl+I"><span class="fa fa-italic"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Heading (Ctrl+H)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdHeading" data-hotkey="Ctrl+H"><span class="fa fa-header"></span> </button></div>
																				            <div class="btn-group"><button class="btn-default btn-sm btn btn-white" type="button" title="URL/Link (Ctrl+L)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdUrl" data-hotkey="Ctrl+L"><span class="fa fa-link"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Image (Ctrl+G)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdImage" data-hotkey="Ctrl+G"><span class="fa fa-picture-o"></span> </button></div>
																				            <div class="btn-group"><button class="btn-default btn-sm btn btn-white" type="button" title="Unordered List (Ctrl+U)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdList" data-hotkey="Ctrl+U"><span class="fa fa-list"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Ordered List (Ctrl+O)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdListO" data-hotkey="Ctrl+O"><span class="fa fa-list-ol"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Code (Ctrl+K)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdCode" data-hotkey="Ctrl+K"><span class="fa fa-code"></span> </button><button class="btn-default btn-sm btn btn-white" type="button" title="Quote (Ctrl+Q)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdQuote" data-hotkey="Ctrl+Q"><span class="fa fa-quote-left"></span> </button></div>
																				            <div class="btn-group"><button class="btn-sm btn btn-primary btn-white" type="button" title="Preview (Ctrl+P)" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdPreview" data-hotkey="Ctrl+P" data-toggle="button"><span class="fa fa-search"></span> Preview</button></div>
																				            <div class="md-controls"><a class="md-control md-control-fullscreen" href="#"><span class="fa fa-expand"></span></a></div>
																				         </div>
																				         <textarea name="menuDis" data-provide="markdown" data-iconlibrary="fa" rows="6" class="md-input" style="resize: none;"><?php echo $menuDis ?></textarea>
																				         <div class="md-fullscreen-controls"><a href="#" class="exit-fullscreen" title="Exit fullscreen"><span class="fa fa-compress"></span></a></div>
																				         <div class="md-fullscreen-controls"><a href="#" class="exit-fullscreen" title="Exit fullscreen"><span class="fa fa-compress"></span></a></div>
																				      </div>
																				   </div>
																				 </div>
																		        </div>
									                                        </div>
									                                </div>
									                                <div class="form-group col-xs-12">
									                                	<div class="col-xs-6" style="position: relative; top:-15px;" > 
		                                                                
																			<div class="form-material floating" >
																			    <label for="status"> Status: </label>
																				<div class="radio">
																					<label>
																						<input class="ace" name="menuStatus" value="1" checked="checked" type="radio">
																						<span class="lbl"> Active</span>
																					</label>
																					<label>
																						<input class="ace" name="menuStatus" value="0" type="radio">
																						<span class="lbl"> Inactive</span>
																					</label>
																				</div>
																			</div>
		                                                                </div>

									                                </div>
		                                                         </div>
																    <br>
	                                                            <div class="modal-footer" style="height: 10%;">
	                                                                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
	                                                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> Update</button>
	                                                            </div>
	                                                        </form>
	                                                    </div> <!-- block-content -->
                                                    
                                                </div>
                                            </div>  <!-- modal-content -->
                                        </div> <!-- modal-dialog -->
                                    </div> <!-- modal fade -->
                                    <!-- END Fade In Modal -->
									<!-- Small Modal -->
					                <div class="modal fade" id="modal-small<?php echo $m++ ?>" tabindex="-1" role="dialog" aria-hidden="true">
					                    <div class="modal-dialog modal-dialog-fromright">
					                        <div class="modal-content">
					                            <div class="modal-header no-padding">
													<div class="table-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
															<span class="white">&times;</span>
														</button>
														Are You Sure ?
													</div>
												</div>
					                            <div class="modal-footer">
					                                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
					                                <a href="contact/delete_action.php?shadow=<?php echo $menuId ?>" class="btn btn-sm btn-primary" type="button"><i class="fa fa-check"></i> Delete</a>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					                <!-- END Small Modal -->
                                   <?php endwhile ?>
								</tbody>
							</table>
						</div><!-- /.span -->
					</div><!-- /.row -->													
				</div><!-- /.page-content -->

				<!-- PAGE CONTENT ENDS -->

			</div> <!-- main-content-inner -->
		</div><!-- /.main-content -->
		

        <?php require_once('common/footer.php') ?>
        <?php require_once('common/script2.php') ?>


	</body>    
</html>
