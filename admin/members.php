<!DOCTYPE html>
<html lang="en">
    <body>
        <div>
            <?php require_once('common/sessionStart.php') ?>

            <?php require_once('common/notAcess.php') ?>

            <?php require_once('common/header.php') ?>

			<?php require_once('common/navList.php') ?>

			<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
				<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
			</div>
		</div>
		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Home</a>
						</li>

						<li>
							<a href="#">Members</a>
						</li>
						<li class="active">List</li>
					</ul><!-- /.breadcrumb -->

					<div class="nav-search" id="nav-search">
						<form class="form-search">
							<span class="input-icon">
								<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
								<i class="ace-icon fa fa-search nav-search-icon"></i>
							</span>
						</form>
					</div><!-- /.nav-search -->
				</div>

				<div class="page-content">
					<!-- setting box -->
					    <?php require_once('common/settings.php') ?>
					<!-- setting box end -->


					<div class="page-header">
						<h1>
							Members
							<small>
								<i class="ace-icon fa fa-angle-double-right"></i>
								List
							</small>
						</h1>
					</div>

					<!-- PAGE CONTENT BEGINS -->
					<div class="row">
						<div class="col-xs-12">
							<table id="simple-table" class="table  table-bordered table-hover">
								<thead>
									<tr>
										<th class="center">
											<label class="pos-rel">
												<input type="checkbox" class="ace" />
												<span class="lbl"></span>
											</label>
										</th>
										<th class="detail-col">No.</th>
										<th>Image</th>
										<th>Username</th>
										<th>Profile Id</th>
										<th class="hidden-480">Sex</th>

										<th>
											<i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
											Email
										</th>
										<th class="hidden-480">Status</th>

										<th class="hidden-480">Action</th>
									</tr>
								</thead>

								<?php 

									$i = 1;
									$j = 1;
									$k = 1;
									$m = 1;
									$s = 1;


									require_once('mysql_connect.php');

									$q           = mysql_query("select * from members");
                                    while ($r    = mysql_fetch_array($q)):

                                   $memId           = $r['memId'];
                                   $memUname 		= $r['memUname'];
                                   $memProId 		= $r['memProId'];
                                   $memSex          = $r['memSex'];
                                   $memEmail        = $r['memEmail'];
                                   $memName         = $r['memName'];
                                   $memAge          = $r['memAge'];
                                   $memReligion     = $r['memReligion'];    
                                   $memImage1		= $r['memImage1'];
                                   $status		    = $r['status'];


                                     ?>

								<tbody>
									<tr>
										<td class="center">
											<label class="pos-rel">
												<input type="checkbox" class="ace" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<a href="#"><?php echo $i++ ?></a>
										</td>
										<td>
											<img style="width: 50px;height: 50px" src="../assets/memImages/<?php echo $memImage1 ?>" >
										</td>

										<td>
											<a href="../viewProfile.php?shadow=<?php echo $memId ?>"><?php echo $memUname ?></a>
										</td>
										<td><?php echo $memProId ?></td>
										<td class="hidden-480"><?php echo $memSex ?></td>
										<td><?php echo $memEmail ?></td>
                                       <?php if ($status==1):?>
										<td class="hidden-480">
											<span class="label label-success arrowed-in arrowed-in-right">Active</span>
											<i style="color: green" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>
										</td>
									<?php else:?>
									    <td class="hidden-480">
											<span class="label label-danger arrowed-in">Inactive</span>
										</td>
									<?php endif;?>


										<td>
											<div class="hidden-sm hidden-xs btn-group">
												

												<button class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal-fadein<?php echo $j++ ?>" >
													<i class="ace-icon fa fa-pencil bigger-120"></i>
												</button>
												&nbsp;

												<a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-small<?php echo $s++ ?>" type="button" title="Remove Client" data-toggle="tooltip" href="#"><i class="fa fa-times"></i></a>

												
											</div>

											<div class="hidden-md hidden-lg">
												<div class="inline pos-rel">
													<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
														<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
													</button>

													<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
														<li>
															<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																<span class="blue">
																	<i class="ace-icon fa fa-search-plus bigger-120"></i>
																</span>
															</a>
														</li>

														<li>
															<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
																<span class="green">
																	<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																</span>
															</a>
														</li>

														<li>
															<a href="#modal-table" class="tooltip-error" data-rel="tooltip" title="Delete">
																<span class="red">
																	<i class="ace-icon fa fa-trash-o bigger-120"></i>
																</span>
															</a>
														</li>
													</ul>
												</div>
											</div>
										</td>
									</tr>
									<!-- Fade In Modal -->
                                    <div class="modal fade"  id="modal-fadein<?php echo $k++ ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="block block-themed block-transparent remove-margin-b">

		                                                <div class="modal-header no-padding">
															<div class="table-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																	<span class="white">&times;</span>
																</button>
																Update Form
															</div>
														</div>

												    
	                                                    <div class="block-content">
	                                                        <form class="form-horizontal push-10-t push-10" action="actions/update_action.php" method="post">
                                                                <div class="modal-body" style="height: 150px;">	
		                                                            <input type="hidden" value="<?php echo $memId ?>"   name="memId">
		                                                            <div class="form-group col-xs-12 ">
		                                                                <div class="col-xs-6">
		                                                                    <div class="form-material floating">
		                                                                        <label for="register3-username">Username :</label>
		                                                                        <input class="form-control" value="<?php echo $memUname ?>" type="text" id="register3-username" name="memUname">    
		                                                                    </div>
		                                                                </div>
		                                                                <div class="col-xs-6">
		                                                                    <div class="form-material floating">
		                                                                        <label for="register3-email"> Sex :</label>
		                                                                        <input class="form-control" value="<?php echo $memSex ?>" type="text" id="register3-email" name="memSex">
		                                                                        
		                                                                    </div>
		                                                                </div>
		                                                            </div>
		                                                            <br>

		                                                            <div class="form-group col-xs-12">
		                                                                <div class="col-xs-6">
		                                                                    <div class="form-material floating">
		                                                                        <label for="register3-password">Email :</label>
		                                                                        <input class="form-control" value="<?php echo $memEmail ?>" type="text" id="register3-password" name="memEmail"> 
		                                                                    </div>
		                                                                </div>

		                                                                <br>

		                                                                <div class="col-xs-6" style="position: relative; top:-15px;" > 
		                                                                
																			<div class="form-material floating" >
																			    <label for="status"> Status: </label>
																				<div class="radio">
																					<label>
																						<input class="ace" name="status" value="1" checked="checked" type="radio">
																						<span class="lbl"> Active</span>
																					</label>
																					<label>
																						<input class="ace" name="status" value="0" type="radio">
																						<span class="lbl"> Inactive</span>
																					</label>
																				</div>
																			</div>
		                                                                </div>
		                                                            </div>
		                                                         </div>
																    <br>
	                                                            <div class="modal-footer" style="height: 60%;">
	                                                                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
	                                                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> Update</button>
	                                                            </div>
	                                                        </form>
	                                                    </div> <!-- block-content -->
                                                    
                                                </div>
                                            </div>  <!-- modal-content -->
                                        </div> <!-- modal-dialog -->
                                    </div> <!-- modal fade -->
                                    <!-- END Fade In Modal -->
									<!-- Small Modal -->
					                <div class="modal fade" id="modal-small<?php echo $m++ ?>" tabindex="-1" role="dialog" aria-hidden="true">
					                    <div class="modal-dialog modal-dialog-fromright">
					                        <div class="modal-content">
					                            <div class="modal-header no-padding">
													<div class="table-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
															<span class="white">&times;</span>
														</button>
														Are You Sure ?
													</div>
												</div>
					                            <div class="modal-footer">
					                                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
					                                <a href="actions/delete_action.php?shadow=<?php echo $memId ?>" class="btn btn-sm btn-primary" type="button"><i class="fa fa-check"></i> Delete</a>
					                            </div>
					                        </div>
					                    </div>
					                </div>
					                <!-- END Small Modal -->
                                   <?php endwhile ?>
								</tbody>
							</table>
						</div><!-- /.span -->
					</div><!-- /.row -->													
				</div><!-- /.page-content -->

				<!-- PAGE CONTENT ENDS -->

			</div> <!-- main-content-inner -->
		</div><!-- /.main-content -->

        <?php require_once('common/footer.php') ?>
        <?php require_once('common/script2.php') ?>


	</body>    
</html>
