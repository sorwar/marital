

<!DOCTYPE html>
<html lang="en">
   <body>
      <div>
         <?php require_once('common/sessionStart.php') ?>
         <?php require_once('common/notAcess.php') ?>
         <?php require_once('common/header.php') ?>
         <?php require_once('common/navList.php') ?>
         <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
         </div>
      </div>
      <div class="main-content">
         <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
               <ul class="breadcrumb">
                  <li>
                     <i class="ace-icon fa fa-home home-icon"></i>
                     <a href="index.php">Home</a>
                  </li>
                  <li>
                     <a href="#">Basic</a>
                  </li>
                  <!-- <li class="active">List</li> -->
               </ul>
               <!-- /.breadcrumb -->
               <div class="nav-search" id="nav-search">
                  <form class="form-search">
                     <span class="input-icon">
                     <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                     <i class="ace-icon fa fa-search nav-search-icon"></i>
                     </span>
                  </form>
               </div>
               <!-- /.nav-search -->
            </div>
            <div class="page-content">
               <!-- setting box -->
               <?php require_once('common/settings.php') ?>
               <!-- setting box end -->
               <div class="page-header">
                  <h1>
                     Menu
                     <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        <!-- List -->
                     </small>
                  </h1>
               </div>
               <div class="row">
                 <!--  <div class="col-xs-12">
                     <div class="widget-box">
                        <div class="widget-header">
                           <button class="btn btn-xs btn-danger pull-right" data-toggle="modal" data-target="#small" style="position: relative;top: 4px;left: -5px;"  >
                           <i class="ace-icon fa fa-plus"></i>
                           <span class="bigger-110">Add New Content</span>
                           </button>
                        </div>
                     </div>
                  </div> -->
                  <!-- Small Modal -->
                  <div class="modal fade" id="small" tabindex="-1" role="dialog" aria-hidden="true" >
                     <div class="modal-dialog modal-dialog-fromright">
                        <div class="modal-content">
                           <div class="widget-box">
                              <div class="widget-header">
                                 <h4 class="widget-title">Basic Form</h4>
                                 <span class="widget-toolbar">
                                 <button class="close" type="button" data-dismiss="modal" aria-hidden="true">
                                 <i class="ace-icon fa fa-times"></i>
                                 </button>
                                 </span>
                              </div>
                              <form  style="height: 500px;" action="basic/action.php" method="post" enctype='multipart/form-data'>
                                 <div class="widget-body">
                                    <div class="widget-main">
                                       <div class="modal-body" style="height: 380px;">
                                          <input type="hidden" value=""   name="Id">
                                          <div class="form-group col-xs-12 ">
                                             <div class="col-xs-6">
                                                <div class="image-preview1" id="image-preview1">
                                                   <label for="image-upload" id="image-label1">Logo</label>
                                                   <input type="file" name="logo" id="image-upload1" />
                                                </div>
                                             </div>
                                             <div class="col-xs-6">
                                                <div class="form-material floating">
                                                   <label for="register3-email"> About :</label>
                                                   <input class="form-control" value="" type="text" id="register3-email" name="about">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group col-xs-12 ">
                                             <div class="col-xs-6">
                                                <div class="image-preview1" id="image-preview1">
                                                   <label for="image-upload" id="image-label1">Background</label>
                                                   <input type="file" name="bgImg" id="image-upload1" />
                                                </div>
                                             </div>
                                             <div class="col-xs-6">
                                                <div class="form-material floating">
                                                   <label for="register3-email"> Facebook Link :</label>
                                                   <input class="form-control" value="" type="text" id="register3-email" name="facebook">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group col-xs-12 ">
                                             <div class="col-xs-6">
                                                <div class="form-material floating">
                                                   <label for="register3-username">Twitter Link :</label>
                                                   <input class="form-control" value="" type="text" id="register3-username" name="twitter">    
                                                </div>
                                             </div>
                                             <div class="col-xs-6">
                                                <div class="form-material floating">
                                                   <label for="register3-email"> GooglePlus Link :</label>
                                                   <input class="form-control" value="" type="text" id="register3-email" name="google">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group col-xs-12 ">
                                             <div class="col-xs-6">
                                                <div class="form-material floating">
                                                   <label for="register3-username">Youtube Link :</label>
                                                   <input class="form-control" value="" type="text" id="register3-username" name="youtube">    
                                                </div>
                                             </div>
                                             <div class="col-xs-6">
                                                <div class="form-material floating">
                                                   <label for="register3-email"> Header Title :</label>
                                                   <input class="form-control" value="" type="text" id="register3-email" name="headTitle">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group col-xs-12 ">
                                             <div class="col-xs-6">
                                                <div class="form-material floating">
                                                   <label for="register3-username">Footer Title :</label>
                                                   <input class="form-control" value="" type="text" id="register3-username" name="footerTitle">    
                                                </div>
                                             </div>
                                             <div class="col-xs-6">
                                                <div class="form-material floating">
                                                   <label for="register3-email"> Map Link :</label>
                                                   <input class="form-control" value="" type="text" id="register3-email" name="map">
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="modal-footer" style="height: 60px;">
                                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> Save</button>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- END Small Modal -->
               </div>
               <!-- Small Modal -->
               <div class="modal fade" id="small<?php echo $m++ ?>" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-fromright">
                     <div class="modal-content">
                        <div class="modal-header no-padding">
                           <div class="table-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              <span class="white">&times;</span>
                              </button>
                              Are You Sure ?
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                           <a href="basic/delete_action.php?shadow=<?php echo $menuId ?>" class="btn btn-sm btn-primary" type="button"><i class="fa fa-check"></i> Delete</a>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- END Small Modal -->
               <!-- PAGE CONTENT BEGINS -->
               <div class="row">
                  <!-- form content  -->
                  <!-- ****form content here *****...  -->
                  <!-- form content end  -->
                  <div class="col-xs-12">
                     <table id="simple-table" class="table  table-bordered table-hover">
                        <thead>
                           <tr>
                              <th class="center">
                                 <label class="pos-rel">
                                 <input type="checkbox" class="ace" />
                                 <span class="lbl"></span>
                                 </label>
                              </th>
                              <th class="detail-col">No.</th>
                              <th>Logo</th> 
                              <th>Background Image</th>
                              <th>Head Title</th>
                              <th>Footer Title</th>
                              <th class="hidden-480 text-center">Status</th>
                              <th style="width: 100px;" class="hidden-480 text-center">Action</th>
                           </tr>
                        </thead>
                        <?php 
                           $i = 1;
                           $j = 1;
                           $k = 1;
                           $m = 1;
                           $s = 1;
                           
                           
                           require_once('mysql_connect.php');
                           
                           $q          = mysql_query("select * from basic");
                                                      while ($r    = mysql_fetch_array($q)):
                           
                                                       $id            = $r['id'];
                                                    	 $logo          = $r['logo'];
                                                    	 $about         = $r['about'];
                                                   	 $background    = $r['background'];
                                 						    $facebook      = $r['facebook'];
                                 						    $twitter       = $r['twitter'];
                                                       $google		    = $r['google'];
                                                       $youtube	    = $r['youtube'];
                                                       $headTitle     = $r['headTitle'];
                                                       $footerTitle   = $r['footerTitle'];
                                                       $map           = $r['map'];
                                                       $status		    = $r['status'];
                           
                           
                                                       ?>
                        <tbody>
                           <tr>
                              <td class="center">
                                 <label class="pos-rel">
                                 <input type="checkbox" class="ace" />
                                 <span class="lbl"></span>
                                 </label>
                              </td>
                              <td>
                                 <a href="#"><?php echo $i++ ?></a>
                              </td>
                              <td>
                                 <img style="width: 50px;height: 50px" src="basic/Images/<?php echo $logo ?>" >
                              </td>
                              <td>
                                 <img style="width: 50px;height: 50px" src="basic/Images/<?php echo $background ?>" >
                              </td>
                              <td><?php echo $headTitle ?></td>
                              <td><?php echo $footerTitle ?></td>

                              <?php if ($status==1):?>
                              <td class="hidden-480">
                                 <span class="label label-success arrowed-in arrowed-in-right">Active</span>
                              </td>
                              <?php else:?>
                              <td class="hidden-480">
                                 <span class="label label-danger arrowed-in">Inactive</span>
                              </td>
                              <?php endif;?>
                              <td class="text-center">
                                 <div class="hidden-sm hidden-xs btn-group">
                                    <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#modal-fadein<?php echo $j++ ?>" >
                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                    </button>
                                    &nbsp;
                                    <!-- <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-small<?php echo $s++ ?>" type="button" title="Remove Client" data-toggle="tooltip" href="#"><i class="fa fa-times"></i></a> -->
                                 </div>
                                 <div class="hidden-md hidden-lg">
                                    <div class="inline pos-rel">
                                       <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                       <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                       </button>
                                       <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                          <li>
                                             <a href="#" class="tooltip-info" data-rel="tooltip" title="View">
                                             <span class="blue">
                                             <i class="ace-icon fa fa-search-plus bigger-120"></i>
                                             </span>
                                             </a>
                                          </li>
                                          <li>
                                             <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
                                             <span class="green">
                                             <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                             </span>
                                             </a>
                                          </li>
                                          <li>
                                             <a href="#modal-table" class="tooltip-error" data-rel="tooltip" title="Delete">
                                             <span class="red">
                                             <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                             </span>
                                             </a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </td>
                           </tr>
                           <!-- Fade In Modal -->
                           <div class="modal fade"  id="modal-fadein<?php echo $k++ ?>" tabindex="-1" role="dialog" aria-hidden="true">
                              <div class="modal-dialog">
                                 <div class="modal-content">
                                    <div class="block block-themed block-transparent remove-margin-b">
                                       <div class="modal-header no-padding">
                                          <div class="table-header">
                                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                             <span class="white">&times;</span>
                                             </button>
                                             Update Form
                                          </div>
                                       </div>
                                       <div class="block-content">
                                          <form " action="basic/updateAction.php" method="post" enctype='multipart/form-data'>
                                             <div class="widget-body">
                                                <div class="widget-main">
                                                   <div class="modal-body" style="height: 380px;">
                                                      <input type="hidden" value="<?php echo $id ?>"   name="id">
                                                      <div class="form-group col-xs-12 ">
                                                         <div class="col-xs-6">
                                                            <div class="image-preview1" id="image-preview1">
                                                               <label for="image-upload" id="image-label1">Logo</label>
                                                               <input type="file" name="logo" id="image-upload1" />
                                                            </div>
                                                         </div>
                                                         <div class="col-xs-6">
                                                            <div class="form-material floating">
                                                               <label for="register3-email"> About :</label>
                                                               <input class="form-control" value="<?php echo $about ?>" type="text" id="register3-email" name="about">
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="form-group col-xs-12 ">
                                                         <div class="col-xs-6">
                                                            <div class="image-preview1" id="image-preview1">
                                                               <label for="image-upload" id="image-label1">Background</label>
                                                               <input type="file" name="background" id="image-upload1" />
                                                            </div>
                                                         </div>
                                                         <div class="col-xs-6">
                                                            <div class="form-material floating">
                                                               <label for="register3-email"> Facebook Link :</label>
                                                               <input class="form-control" value="<?php echo $facebook ?>" type="text" id="register3-email" name="facebook">
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="form-group col-xs-12 ">
                                                         <div class="col-xs-6">
                                                            <div class="form-material floating">
                                                               <label for="register3-username">Twitter Link :</label>
                                                               <input class="form-control" value="<?php echo $twitter ?>" type="text" id="register3-username" name="twitter">    
                                                            </div>
                                                         </div>
                                                         <div class="col-xs-6">
                                                            <div class="form-material floating">
                                                               <label for="register3-email"> GooglePlus Link :</label>
                                                               <input class="form-control" value="<?php echo $google ?>" type="text" id="register3-email" name="google">
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="form-group col-xs-12 ">
                                                         <div class="col-xs-6">
                                                            <div class="form-material floating">
                                                               <label for="register3-username">Youtube Link :</label>
                                                               <input class="form-control" value="<?php echo $youtube ?>" type="text" id="register3-username" name="youtube">    
                                                            </div>
                                                         </div>
                                                         <div class="col-xs-6">
                                                            <div class="form-material floating">
                                                               <label for="register3-email"> Header Title :</label>
                                                               <input class="form-control" value="<?php echo $headTitle ?>" type="text" id="register3-email" name="headTitle">
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="form-group col-xs-12 ">
                                                         <div class="col-xs-6">
                                                            <div class="form-material floating">
                                                               <label for="register3-username">Footer Title :</label>
                                                               <input class="form-control" value="<?php echo $footerTitle ?>" type="text" id="register3-username" name="footerTitle">    
                                                            </div>
                                                         </div>
                                                         <div class="col-xs-6">
                                                            <div class="form-material floating">
                                                               <label for="register3-username">Map Link :</label>
                                                               <input class="form-control" value="<?php echo $map ?>" type="text" id="register3-username" name="map">    
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="modal-footer">
                                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> Update</button>
                                             </div>
                                          </form>
                                       </div>
                                       <!-- block-content -->
                                    </div>
                                 </div>
                                 <!-- modal-content -->
                              </div>
                              <!-- modal-dialog -->
                           </div>
                           <!-- modal fade -->
                           <!-- END Fade In Modal -->
                           <!-- Small Modal -->
                           <div class="modal fade" id="modal-small<?php echo $m++ ?>" tabindex="-1" role="dialog" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-fromright">
                                 <div class="modal-content">
                                    <div class="modal-header no-padding">
                                       <div class="table-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                          <span class="white">&times;</span>
                                          </button>
                                          Are You Sure ?
                                       </div>
                                    </div>
                                    <div class="modal-footer">
                                       <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                                       <a href="basic/delete_action.php?shadow=<?php echo $id ?>" class="btn btn-sm btn-primary" type="button"><i class="fa fa-check"></i> Delete</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- END Small Modal -->
                           <?php endwhile ?>
                        </tbody>
                     </table>
                  </div>
                  <!-- /.span -->
               </div>
               <!-- /.row -->													
            </div>
            <!-- /.page-content -->
            <!-- PAGE CONTENT ENDS -->
         </div>
         <!-- main-content-inner -->
      </div>
      <!-- /.main-content -->
      <?php require_once('common/footer.php') ?>
      <?php require_once('common/script2.php') ?>
   </body>
</html>

